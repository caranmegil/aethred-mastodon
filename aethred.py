#    aethred.py - Bot script
#    Copyright (C) 2020  William R. Moore <caranmegil@gmail.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program.  If not, see <https://www.gnu.org/licenses/>.

import re
import requests
import json
from mastodon import StreamListener, Mastodon

with open('../config/aethred-mastodon.json', 'r') as file:
    data = file.read()
    config = json.loads(data)

masto = Mastodon(
        access_token=config['CONFIG'],
        api_base_url = 'https://' + config['HOST']
)

HAZARD_RE = re.compile('.*\!hazard ([5-9]).*')

class TootStream(StreamListener):
    def on_update(self, status):
        if 'https://' + config['HOST'] + '/@' + config['NAME'] in status['content']:
            g = HAZARD_RE.match(status['content'])
            
            if not g == None:
                r = requests.get(config['HAZARD_HOST'] + '/' + g[1])
                j = r.json()
                if j['result'] == 0:
                    masto.status_post('@' + status['account']['username'] + 'You nicked it!  Way to go!', in_reply_to_id=status['id'], visibility=status['visibility'])
                else:
                    masto.status_post('@' + status['account']['username'] + 'Sorry, you\'re out!', in_reply_to_id=status['id'], visibility=status['visibility'])

            else:
                r = requests.post(config['LINGUA_HOST'], data=json.dumps({'text': status['content']}))
                masto.status_post('@' + status['account']['acct'] + ' '  + ' '.join(r.json()['response']), in_reply_to_id=status['id'], visibility=status['visibility'])

masto.stream_user(TootStream())
